# Browser Performance Testing Demo

This is a demo of the GitLab [Browser Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html) feature.

NOTE:
If the Browser Performance report has no data to compare, such as when you add the
Browser Performance job in your `.gitlab-ci.yml` for the very first time,
the Browser Performance report widget doesn't display. It must have run at least
once on the target branch (`main`, for example), before it displays in a
merge request targeting that branch.

## Repository Structure

Browser Performance Testing requires a static site and a deployed environment in order to work.
This repository uses the following tools to achieve that.

### Gatsby

This project uses [Gatsby](https://www.gatsbyjs.com/) to build a static site. The current site is the default one
automatically generated when you initialize a new project.

### Surge.sh

In order to deploy review apps and a staging environment, this project is using [Surge.sh](https://surge.sh/) to easily host a static site.
